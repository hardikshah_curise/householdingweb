﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU.HouseHolding.DataAccess.Model
{
    public class ViewReportModel
    {
        public string HouseHoldId { get; set; }
        public string MemberId { get; set; }
        public string MemberName { get; set; }
        public List<string> ReportNames { get; set; }
        public string HOHId { get; set; }
        public List<SearchResults> lstResults { get; set; }
    }

    public class SearchResults
    {
        public string HouseHoldId { get; set; }
        public string MemberId { get; set; }
        public string MemberName { get; set; }
        public string Address { get; set; }
    }

    public class SummrybyMembers
    { 
        public List<SummaryResult> lstResults { get; set; }
        public List<ChartModel> ChartData { get; set; }

    }
    public class SummryDetailViewResult
    {
        public DataTable lstResults { get; set; }
    }
    public class SummaryResult
    {
        public string Description { get; set; }
        public string Tier1 { get; set; }
        public string Tier2 { get; set; }
        public string Tier3 { get; set; }
        public string Tier4 { get; set; }
        public string Tier5 { get; set; }
        public string Total { get; set; }

    }

    public class ChartModel
    {
        public string Series { get; set; }
        public string Values { get; set; }
    }
    public class HouseHoldSummaryViewResult
    { 
        public List<ImportedFileDetail> lstMembers { get; set; }
        public HouseHoldSummaryDetails HHSummaryDetail { get; set; }
    }
    public class HouseHoldSummaryDetails
    { 
        public int TotalMembers { get; set; }
        public int TotalProducts { get; set; }
        public int TotalLoanProducts { get; set; }
        public int TotalDepositProducts { get; set; }
        public decimal TotalTransactionAmount { get; set; }
        public int TotalTransactionNo { get; set; }
        public decimal TotalCheckingBalance { get; set; }
        public decimal TotalSavingBalance { get; set; }
        public decimal AvgCheckingBalance { get; set; }
        public decimal AvgSavingBalance { get; set; }
        public string HeadofHouseHoldId { get; set; }
    }
}
