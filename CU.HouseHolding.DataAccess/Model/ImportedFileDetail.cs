﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU.HouseHolding.DataAccess.Model
{
    public class ImportedFileDetail
    {
        public int Id { get; set; }

        public int ImportedFileid { get; set; }

        public string Memberid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string PhoneNo { get; set; }
        public string JoinMemberId { get; set; }
        public int? Age { get; set; }

        public int? TenureInCU { get; set; }

        public int? TotalProducts { get; set; }

        public string CheckingAccount { get; set; }

        public decimal? CheckingAccountBalance { get; set; }

        public int? TotalTransactionInLast6Month { get; set; }

        public decimal? TotalTransactionAmountInLast6Month { get; set; }

        public string MortgageAccount { get; set; }

        public decimal? MortgageBalanceLeft { get; set; }

        public int? MortgageTermLeft { get; set; }

        public string SavingAccount { get; set; }

        public decimal? SavingAccountBalance { get; set; }

        public string HouseHoldId { get; set; }
        public string MappedMemberId { get; set; }
        public int? MatchedPercentage { get; set; }
        public int? Priority { get; set; }
        public bool IsMatch { get; set; }
        public string Pattern1 { get; set; }
        public string Pattern2 { get; set; }
        public string Pattern3 { get; set; }
        public string Pattern4 { get; set; }
        public string Pattern5 { get; set; }
        //public string Pattern1 { get { return LastName + " " + Address1 + " " + Address2 + " " + City + " " + State + " " + (Zip.Length > 5 ? Zip.Substring(0, 4) : Zip); } }//LastName +Address1 + Addresss2 +City +State+Zip 
        //public string Pattern2 { get { return LastName + " " + Address1 + " " + Address2 + " " + State + " " + (Zip.Length > 5 ? Zip.Substring(0, 4) : Zip); } } //LastName +Address1 + Addresss2 +State+Zip 
        //public string Pattern3 { get { return LastName + " " + Address1 + " " + Address2 + " " + City + " " + (Zip.Length > 5 ? Zip.Substring(0, 4) : Zip); } }//LastName +Address1 + Addresss2 +City+Zip 
        //public string Pattern4 { get { return Address1 + " " + Address2 + " " + City + " " + (Zip.Length > 5 ? Zip.Substring(0, 4) : Zip); } }//Address1 + Addresss2 +City+Zip 
        //public string Pattern5 { get { return Address1 + " " + Address2 + " " + State + " " + (Zip.Length > 5 ? Zip.Substring(0, 4) : Zip); } }//Address1 + Addresss2 +State+Zip 
    }

    public class ExportFileDetail
    {
        public string Memberid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public string HouseHoldId { get; set; }
        public string PhoneNo { get; set; }
        public string MappedMemberId { get; set; }
        public string JoinMemberId { get; set; }
    }

    public class ValidateFileResult
    {
        public ValidateFileResult()
        {
            Message = new List<string>();
        }
        public List<string> Message { get; set; }
    }

}
