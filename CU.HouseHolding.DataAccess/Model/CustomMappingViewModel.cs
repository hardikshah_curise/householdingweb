﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU.HouseHolding.DataAccess.Model
{
    public class CustomMappingViewModel
    {
        public CustomMappingViewModel()
        {
            HouseHoldIds = new List<string>();
        }
        public List<string> ReportNames { get; set; }
        public List<string> HouseHoldIds { get; set; }
        public List<ImportedFileDetail> MemberIds { get; set; }
        public List<ImportedFileDetail> UnAllocatedMemberIds { get; set; }
        public List<string> Cities { get; set; }
        public List<string> States { get; set; }
        public List<string> Zipcodes { get; set; }
        public string MemberName { get; set; }
        public string HHId { get; set; }
    }
}
