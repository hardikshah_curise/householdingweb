﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CU.HouseHolding.DataAccess.Model
{
    public class ImportedFileMaster
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public int UserId { get; set; }

        public int SimilarityScore { get; set; }

        public string ReportName { get; set; }

        public DateTime? ProcessStartDate { get; set; }

        public DateTime? ProcessEndDate { get; set; }

        public DateTime CreatedDate { get; set; }
    }
    public class ImportedFileMasterView
    {
        public ImportedFileMasterView() {
            Page = 1;
            PageSize = 100;
        }
        public int Page { get; set; }
        public int Pages { get; set; }
        public int PageSize { get; set; }
        public int Rows { get; set; }
        public List<ImportedFileMaster> FileMstResults { get; set; }
        public List<ImportedFileDetail> FileDtlResults { get; set; }
        public List<int> PageNumbers {
            get {
                List<int> l = new List<int>();
                for (int i = 1; i <= Pages; i++)
                {
                    l.Add(i);
                }
                return l;
            }
        }
        public List<int> PageSteps
        {
            get
            {
                List<int> l = new List<int>();
                int i = 100;
                while (i<500)
                {
                    l.Add(i);
                    i += 50;
                }
                return l;
            }
        }
    }
}
