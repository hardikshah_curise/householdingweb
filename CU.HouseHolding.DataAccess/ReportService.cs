﻿using CU.HouseHolding.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF = CU.HouseHolding.Model;
namespace CU.HouseHolding.DataAccess
{
    public class ReportService : ConnectionHelper
    {
        EF.HouseHoldingEntities db = null;
        public ReportService()
        {
            db = new EF.HouseHoldingEntities(EntityConnectionString);
        }
        public ReportService(ObjectContext context)
        {
            db = context as EF.HouseHoldingEntities;
        }
        public ObjectContext DbContext
        {
            get
            {
                return db;
            }
        }

        public List<SummaryResult> GetSummarybyMembers()
        {
            DataHelper ds = new DataHelper();
            List<SummaryResult> lst= ds.ExecuteStoredProcedure<SummaryResult>("proc_GetHHSummryByMembers", null);
            return lst;
        }
        public List<ChartModel> GetSummarybyMembersChartData()
        {
            DataHelper ds = new DataHelper();
            List<ChartModel> lst = ds.ExecuteStoredProcedure<ChartModel>("proc_GetHHSummryByMembers_ChartData", null);
            return lst;
        }
        public List<ChartModel> GetMetricsChartData(int TierGroup)
        {
            DataHelper ds = new DataHelper();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@TierGroup", TierGroup);
            List<ChartModel> lst = ds.ExecuteStoredProcedure<ChartModel>("proc_GetTierMetricsDetails_ChartData", param);
            return lst;
        }
        public List<SummaryResult> GetSummarybyProducts()
        {
            DataHelper ds = new DataHelper();
            List<SummaryResult> lst = ds.ExecuteStoredProcedure<SummaryResult>("proc_GetHHSummryByProducts", null);
            return lst;
        }
        public DataTable GetSummaryDetails(string Type,int TierGroup)
        {
            DataHelper ds = new DataHelper();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@Type", Type);
            param[1] = new SqlParameter("@Tier", TierGroup);
            DataTable dt = ds.ExecuteStoredProcedureDataTable("proc_GetHouseHoldTierSummary", param);
            return dt;
        }
        public HouseHoldSummaryDetails GetHouseHoldSummarybyHouseHoldId(string HouseHoldId)
        {
            DataHelper ds = new DataHelper();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@HouseHoldId", HouseHoldId);
            HouseHoldSummaryDetails lst = ds.ExecuteStoredProcedure<HouseHoldSummaryDetails>("proc_GetHouseHoldSummary", param).FirstOrDefault();
            return lst;
        }
    }
}
