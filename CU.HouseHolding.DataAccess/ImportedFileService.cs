﻿using CU.HouseHolding.DataAccess.Model;
using CU.HouseHolding.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Cache;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EF = CU.HouseHolding.Model;

namespace CU.HouseHolding.DataAccess
{
    public class ImportedFileService : ConnectionHelper
    {
        EF.HouseHoldingEntities db = null;
        public ImportedFileService()
        {
            db = new EF.HouseHoldingEntities(EntityConnectionString);
        }
        public ImportedFileService(ObjectContext context)
        {
            db = context as EF.HouseHoldingEntities;
        }
        public ObjectContext DbContext
        {
            get
            {
                return db;
            }
        }

        #region FileMaster

        public IQueryable<ImportedFileMaster> ImportedFileMasters
        {
            get
            {
                return from r in db.ImportedFileMaster
                       select new ImportedFileMaster
                       {
                           Id = r.Id,
                           FileName = r.FileName,
                           UserId = r.UserId,
                           SimilarityScore = r.SimilarityScore,
                           ReportName = r.ReportName,
                           ProcessStartDate = r.ProcessStartDate,
                           ProcessEndDate = r.ProcessEndDate,
                           CreatedDate = r.CreatedDate,
                       };
            }
        }
        public void ImportedFileMaster_InsertOrUpdate(ImportedFileMaster r)
        {
            if (r.Id == 0)
            {
                var i = new EF.ImportedFileMaster
                {
                    FileName = r.FileName,
                    UserId = r.UserId,
                    SimilarityScore = r.SimilarityScore,
                    ReportName = r.ReportName,
                    ProcessStartDate = r.ProcessStartDate,
                    ProcessEndDate = r.ProcessEndDate,
                    CreatedDate = r.CreatedDate,
                };

                db.ImportedFileMaster.AddObject(i);
                db.SaveChanges();
                r.Id = i.Id;
            }


            else
            {
                var u = db.ImportedFileMaster.Where(p => p.Id == r.Id).Single();
                u.FileName = r.FileName;
                u.UserId = r.UserId;
                u.SimilarityScore = r.SimilarityScore;
                u.ReportName = r.ReportName;
                u.ProcessStartDate = r.ProcessStartDate;
                u.ProcessEndDate = r.ProcessEndDate;
                u.CreatedDate = r.CreatedDate;

                db.SaveChanges();
            }
        }
        public bool CheckReportExist(string ReportName)
        {
            var a = db.ImportedFileMaster.Where(p => p.ReportName == ReportName).FirstOrDefault();
            if (a == null)
            {
                return false;
            }

            return true;
        }
        #endregion

        #region FileDetails
        public IQueryable<ImportedFileDetail> ImportedFileDetails
        {
            get
            {
                return from r in db.ImportedFileDetail
                       select new ImportedFileDetail
                       {
                           Id = r.Id,
                           ImportedFileid = r.ImportedFileid,
                           Memberid = r.Memberid,
                           FirstName = r.FirstName,
                           LastName = r.LastName,
                           Address1 = r.Address1,
                           Address2 = r.Address2,
                           City = r.City,
                           State = r.State,
                           Zip = r.Zip,
                           PhoneNo = r.PhoneNo,
                           MappedMemberId = r.MappedMemberId,
                           MatchedPercentage = r.MatchedPercentage,
                           Priority = r.Priority,
                           Age = r.Age,
                           TenureInCU = r.TenureInCU,
                           TotalProducts = r.TotalProducts,
                           CheckingAccount = r.CheckingAccount,
                           CheckingAccountBalance = r.CheckingAccountBalance,
                           TotalTransactionInLast6Month = r.TotalTransactionInLast6Month,
                           TotalTransactionAmountInLast6Month = r.TotalTransactionAmountInLast6Month,
                           MortgageAccount = r.MortgageAccount,
                           MortgageBalanceLeft = r.MortgageBalanceLeft,
                           MortgageTermLeft = r.MortgageTermLeft,
                           SavingAccount = r.SavingAccount,
                           SavingAccountBalance = r.SavingAccountBalance,
                           HouseHoldId = r.HouseHoldId
                       };
            }
        }
        public List<ImportedFileDetail> GetFileDetails(int FileId)
        {

            var lst = (from r in db.ImportedFileDetail
                       where r.ImportedFileid == FileId
                       select new ImportedFileDetail
                       {
                           Id = r.Id,
                           ImportedFileid = r.ImportedFileid,
                           Memberid = r.Memberid,
                           FirstName = r.FirstName,
                           LastName = r.LastName,
                           Address1 = r.Address1,
                           Address2 = r.Address2,
                           City = r.City,
                           State = r.State,
                           Zip = r.Zip,
                           PhoneNo = r.PhoneNo,
                           MappedMemberId = (string.IsNullOrEmpty(r.MappedMemberId) ? "HH" + r.Memberid : r.MappedMemberId),
                           MatchedPercentage = r.MatchedPercentage,
                           Priority = r.Priority,
                           HouseHoldId = r.HouseHoldId,
                           JoinMemberId = r.JoinMemberId
                       }).ToList();
            return lst;
        }
        public List<ImportedFileDetail> GetMemberDetailsbyHouseHoldId(string HouseHoldId)
        {

            var lst = (from r in db.ImportedFileDetail
                       where r.HouseHoldId == HouseHoldId
                       select new ImportedFileDetail
                       {
                           Id = r.Id,
                           ImportedFileid = r.ImportedFileid,
                           Memberid = r.Memberid,
                           FirstName = r.FirstName,
                           LastName = r.LastName,
                           Address1 = r.Address1,
                           Address2 = r.Address2,
                           City = r.City,
                           State = r.State,
                           Zip = r.Zip,
                           PhoneNo = r.PhoneNo,
                           MappedMemberId = (string.IsNullOrEmpty(r.MappedMemberId) ? "HH" + r.Memberid : r.MappedMemberId),
                           MatchedPercentage = r.MatchedPercentage,
                           Priority = r.Priority,
                           HouseHoldId = r.HouseHoldId,
                           JoinMemberId = r.JoinMemberId,
                           TotalProducts=r.TotalProducts,
                           TotalTransactionInLast6Month=r.TotalTransactionInLast6Month,
                           SavingAccountBalance=r.SavingAccountBalance,
                           CheckingAccountBalance=r.CheckingAccountBalance,
                           Age=r.Age
                       }).ToList();
            return lst;
        }

        public List<ExportFileDetail> ExportFileDetails(int ImportedFileId)
        {
            var lst = (from r in db.ImportedFileDetail
                       where r.ImportedFileid == ImportedFileId
                       select new ExportFileDetail
                       {
                           Memberid = r.Memberid,
                           FirstName = r.FirstName,
                           LastName = r.LastName,
                           Address1 = r.Address1,
                           Address2 = r.Address2,
                           City = r.City,
                           State = r.State,
                           Zip = r.Zip,
                           PhoneNo = r.PhoneNo,
                           MappedMemberId = r.MappedMemberId,
                       }).ToList();
            return lst;
        }

        public void ImportedFileDetail_InsertOrUpdate(ImportedFileDetail r)
        {
            if (r.Id == 0)
            {
                var i = new EF.ImportedFileDetail
                {
                    ImportedFileid = r.ImportedFileid,
                    Memberid = r.Memberid,
                    FirstName = r.FirstName,
                    LastName = r.LastName,
                    Address1 = r.Address1,
                    Address2 = r.Address2,
                    City = r.City,
                    State = r.State,
                    Zip = r.Zip,
                    PhoneNo = r.PhoneNo,
                    MappedMemberId = r.MappedMemberId,
                    MatchedPercentage = r.MatchedPercentage,
                    Priority = r.Priority
                };

                db.ImportedFileDetail.AddObject(i);
                db.SaveChanges();
                r.Id = i.Id;
            }


            else
            {
                var u = db.ImportedFileDetail.Where(p => p.Id == r.Id).Single();
                u.ImportedFileid = r.ImportedFileid;
                u.Memberid = r.Memberid;
                u.FirstName = r.FirstName;
                u.LastName = r.LastName;
                u.Address1 = r.Address1;
                u.Address2 = r.Address2;
                u.City = r.City;
                u.State = r.State;
                u.Zip = r.Zip;
                u.PhoneNo = r.PhoneNo;
                u.MappedMemberId = r.MappedMemberId;
                u.MatchedPercentage = r.MatchedPercentage;
                u.Priority = r.Priority;
                db.SaveChanges();
            }
        }
        //public void ImportFileDetail_UpdateList(List<ImportedFileDetail> result)
        //{
        //    using (var ctx = new EF.HouseHoldingEntities(EntityConnectionString))
        //    {
        //        var query = (from ba in db.ImportedFileDetail
        //                    where result.Select(s => s.Memberid).ToArray().Contains(ba.Memberid)
        //                    select new EF.ImportedFileDetail { ba.HouseHoldId, ba.Memberid }).to;
        //        foreach (var ta in query)
        //        {
        //            ta.HouseHoldId = result.Where(p => p.Memberid == ta.Memberid).FirstOrDefault().HouseHoldId;
        //        }
        //        ctx.SaveChanges();
        //    }
        //}
        #endregion
        public void ProcessFile(int FileId = 0)
        {
            if (FileId > 0)
            {
                var FileMaster = db.ImportedFileMaster.Where(p => p.Id == FileId && p.ProcessStartDate == null).FirstOrDefault();
                if (FileMaster != null)
                {
                    FileMaster.ProcessStartDate = DateTime.UtcNow;
                    db.SaveChanges();
                    var FileData = GetFileDetails(FileId); //get uploaded data list from DB
                                                           //list.Where(w => w.Name == "height").ToList().ForEach(s => s.Value = 30);
                                                           //foreach (ImportedFileDetail dtlnew in FileData)
                                                           //{
                                                           //    dtlnew.HouseHoldId = "HH" + dtlnew.Memberid;
                                                           //}
                    var CompareList = FileData.Where(p => p.IsMatch == false).ToList();  //
                    Parallel.ForEach(FileData, dtl =>
                    {
                        Parallel.ForEach(CompareList, Comparedata =>
                        {
                            if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                            {
                                if (Comparedata.JoinMemberId != null)
                                {
                                    if (Comparedata.JoinMemberId.Contains(dtl.Memberid))
                                    {
                                        Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                        Comparedata.Priority = 0;
                                        Comparedata.MatchedPercentage = 100;
                                        Comparedata.IsMatch = true;
                                        if (dtl.IsMatch == false)
                                        {
                                            dtl.IsMatch = true;
                                        }
                                    }
                                }

                            }
                        });

                        string Pattern1 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        Parallel.ForEach(CompareList, Comparedata =>
                        {
                            if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                            {
                                string ComparePattern1 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                                int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern1, ComparePattern1);
                                if (CompareScore > FileMaster.SimilarityScore)
                                {
                                    String s1 = new String(Pattern1.Where(Char.IsDigit).ToArray());
                                    String s2 = new String(ComparePattern1.Where(Char.IsDigit).ToArray());
                                    if (s1 == s2)
                                    {
                                        Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                        Comparedata.Priority = 1;
                                        Comparedata.MatchedPercentage = CompareScore;
                                        Comparedata.IsMatch = true;
                                        if (dtl.IsMatch == false)
                                        {
                                            dtl.IsMatch = true;
                                        }
                                    }
                                }
                            }
                        });

                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        string Pattern2 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //LastName +Address1 + Addresss2 +State+Zip 
                        Parallel.ForEach(CompareList, Comparedata =>
                            {
                                if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                                {
                                    string ComparePattern2 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                                    int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern2, ComparePattern2);
                                    if (CompareScore > FileMaster.SimilarityScore)
                                    {
                                        String s1 = new String(Pattern2.Where(Char.IsDigit).ToArray());
                                        String s2 = new String(ComparePattern2.Where(Char.IsDigit).ToArray());
                                        if (s1 == s2)
                                        {
                                            Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                            Comparedata.Priority = 2;
                                            Comparedata.MatchedPercentage = CompareScore;
                                            Comparedata.IsMatch = true;
                                            if (dtl.IsMatch == false)
                                            {
                                                dtl.IsMatch = true;
                                            }
                                        }
                                    }
                                }
                            });

                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        string Pattern3 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //LastName +Address1 + Addresss2 +State+Zip 
                        Parallel.ForEach(CompareList, Comparedata =>
                            {
                                if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                                {
                                    string ComparePattern3 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                                    int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern3, ComparePattern3);
                                    if (CompareScore > FileMaster.SimilarityScore)
                                    {
                                        String s1 = new String(Pattern3.Where(Char.IsDigit).ToArray());
                                        String s2 = new String(ComparePattern3.Where(Char.IsDigit).ToArray());
                                        if (s1 == s2)
                                        {
                                            Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                            Comparedata.Priority = 3;
                                            Comparedata.MatchedPercentage = CompareScore;
                                            Comparedata.IsMatch = true;
                                            if (dtl.IsMatch == false)
                                            {
                                                dtl.IsMatch = true;
                                            }
                                        }
                                    }
                                }
                            });

                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        string Pattern4 = dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); // Address1 + Addresss2 +City+Zip 
                        Parallel.ForEach(CompareList, Comparedata =>
                            {
                                if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                                {
                                    string ComparePattern4 = Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                                    int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern4, ComparePattern4);
                                    if (CompareScore > FileMaster.SimilarityScore)
                                    {
                                        String s1 = new String(Pattern4.Where(Char.IsDigit).ToArray());
                                        String s2 = new String(ComparePattern4.Where(Char.IsDigit).ToArray());
                                        if (s1 == s2)
                                        {
                                            Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                            Comparedata.Priority = 4;
                                            Comparedata.MatchedPercentage = CompareScore;
                                            Comparedata.IsMatch = true;
                                            if (dtl.IsMatch == false)
                                            {
                                                dtl.IsMatch = true;
                                            }
                                        }
                                    }
                                }
                            });

                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        string Pattern5 = dtl.Address1 + " " + dtl.Address2 + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); // Address1 + Addresss2 +State+Zip 
                        Parallel.ForEach(CompareList, Comparedata =>
                            {
                                if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                                {
                                    string ComparePattern5 = Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                                    int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern5, ComparePattern5);
                                    if (CompareScore > FileMaster.SimilarityScore)
                                    {
                                        String s1 = new String(Pattern5.Where(Char.IsDigit).ToArray());
                                        String s2 = new String(ComparePattern5.Where(Char.IsDigit).ToArray());
                                        if (s1 == s2)
                                        {
                                            Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                            Comparedata.Priority = 5;
                                            Comparedata.MatchedPercentage = CompareScore;
                                            Comparedata.IsMatch = true;
                                            if (dtl.IsMatch == false)
                                            {
                                                dtl.IsMatch = true;
                                            }
                                        }
                                    }
                                }
                            });
                        //Match just Phoneno for remaining Members
                        CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                        Parallel.ForEach(CompareList, Comparedata =>
                        {
                            if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                            {
                                if (!string.IsNullOrEmpty( dtl.PhoneNo) && !string.IsNullOrEmpty(Comparedata.PhoneNo))
                                {
                                    int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(dtl.PhoneNo, Comparedata.PhoneNo);
                                    if (CompareScore > FileMaster.SimilarityScore)
                                    {
                                        Comparedata.MappedMemberId = "HH" + dtl.Memberid;
                                        Comparedata.Priority = 6;
                                        Comparedata.MatchedPercentage = CompareScore;
                                        Comparedata.IsMatch = true;
                                        if (dtl.IsMatch == false)
                                        {
                                            dtl.IsMatch = true;
                                        }

                                    }
                                }
                            }
                        });
                    });
                    #region Commented old Logic
                    //foreach (ImportedFileDetail dtl in FileData)
                    //{
                    //    //CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                    //    string Pattern1 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //    foreach (ImportedFileDetail Comparedata in FileData) // FOraeach loop to Compare with Each record
                    //    {
                    //        if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                    //        {
                    //            string ComparePattern1 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //            int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(Pattern1, ComparePattern1);
                    //            if (CompareScore > FileMaster.SimilarityScore)
                    //            {
                    //                //String s1 = new String(Pattern1.Where(Char.IsDigit).ToArray());
                    //                //String s2 = new String(ComparePattern1.Where(Char.IsDigit).ToArray());
                    //                //if (s1 == s2)
                    //                //{
                    //                Comparedata.HouseHoldId = "HH" + dtl.Memberid;
                    //                Comparedata.Priority = 0;
                    //                Comparedata.MatchedPercentage = CompareScore;
                    //                Comparedata.IsMatch = true;
                    //                if (dtl.IsMatch == false)
                    //                {
                    //                    dtl.IsMatch = true;
                    //                }
                    //                //}
                    //            }
                    //        }
                    //    }
                    //    //CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                    //    // dtl.Pattern2 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //LastName +Address1 + Addresss2 +State+Zip 
                    //    //foreach (ImportedFileDetail Comparedata in CompareList) // FOraeach loop to Compare with Each record
                    //    //{
                    //    //    if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                    //    //    {
                    //    //        Comparedata.Pattern2 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //    //        int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(dtl.Pattern2, Comparedata.Pattern2);
                    //    //        if (CompareScore > FileMaster.SimilarityScore)
                    //    //        {
                    //    //            String s1 = new String(dtl.Pattern2.Where(Char.IsDigit).ToArray());
                    //    //            String s2 = new String(Comparedata.Pattern2.Where(Char.IsDigit).ToArray());
                    //    //            if (s1 == s2)
                    //    //            {
                    //    //                Comparedata.HouseHoldId = "HH" + dtl.Memberid;
                    //    //                Comparedata.Priority = 1;
                    //    //                Comparedata.MatchedPercentage = CompareScore;
                    //    //                Comparedata.IsMatch = true;
                    //    //                if (dtl.IsMatch == false)
                    //    //                {
                    //    //                    dtl.IsMatch = true;
                    //    //                }
                    //    //            }
                    //    //        }
                    //    //    }
                    //    //}
                    //    //CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                    //    //dtl.Pattern3 = dtl.LastName + " " + dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); //LastName +Address1 + Addresss2 +State+Zip 
                    //    //foreach (ImportedFileDetail Comparedata in CompareList) // FOraeach loop to Compare with Each record
                    //    //{
                    //    //    if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                    //    //    {
                    //    //        Comparedata.Pattern3 = Comparedata.LastName + " " + Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //    //        int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(dtl.Pattern3, Comparedata.Pattern3);
                    //    //        if (CompareScore > FileMaster.SimilarityScore)
                    //    //        {
                    //    //            String s1 = new String(dtl.Pattern3.Where(Char.IsDigit).ToArray());
                    //    //            String s2 = new String(Comparedata.Pattern3.Where(Char.IsDigit).ToArray());
                    //    //            if (s1 == s2)
                    //    //            {
                    //    //                Comparedata.HouseHoldId = "HH" + dtl.Memberid;
                    //    //                Comparedata.Priority = 2;
                    //    //                Comparedata.MatchedPercentage = CompareScore;
                    //    //                Comparedata.IsMatch = true;
                    //    //                if (dtl.IsMatch == false)
                    //    //                {
                    //    //                    dtl.IsMatch = true;
                    //    //                }
                    //    //            }
                    //    //        }
                    //    //    }
                    //    //}
                    //    //CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                    //    //dtl.Pattern4 = dtl.Address1 + " " + dtl.Address2 + " " + dtl.City + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); // Address1 + Addresss2 +City+Zip 
                    //    //foreach (ImportedFileDetail Comparedata in CompareList) // FOraeach loop to Compare with Each record
                    //    //{
                    //    //    if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                    //    //    {
                    //    //        Comparedata.Pattern4 = Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.City + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //    //        int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(dtl.Pattern4, Comparedata.Pattern4);
                    //    //        if (CompareScore > FileMaster.SimilarityScore)
                    //    //        {
                    //    //            String s1 = new String(dtl.Pattern4.Where(Char.IsDigit).ToArray());
                    //    //            String s2 = new String(Comparedata.Pattern4.Where(Char.IsDigit).ToArray());
                    //    //            if (s1 == s2)
                    //    //            {
                    //    //                Comparedata.HouseHoldId = "HH" + dtl.Memberid;
                    //    //                Comparedata.Priority = 3;
                    //    //                Comparedata.MatchedPercentage = CompareScore;
                    //    //                Comparedata.IsMatch = true;
                    //    //                if (dtl.IsMatch == false)
                    //    //                {
                    //    //                    dtl.IsMatch = true;
                    //    //                }
                    //    //            }
                    //    //        }
                    //    //    }
                    //    //}
                    //    //CompareList = CompareList.Where(p => p.IsMatch == false).ToList();
                    //    //dtl.Pattern5 = dtl.Address1 + " " + dtl.Address2 + " " + dtl.State + " " + (dtl.Zip.Length > 5 ? dtl.Zip.Substring(0, 4) : dtl.Zip); // Address1 + Addresss2 +State+Zip 
                    //    //foreach (ImportedFileDetail Comparedata in CompareList) // FOraeach loop to Compare with Each record
                    //    //{
                    //    //    if (Comparedata.Memberid != dtl.Memberid && Comparedata.IsMatch == false)  // Match if Both the data are different 
                    //    //    {
                    //    //        Comparedata.Pattern5 = Comparedata.Address1 + " " + Comparedata.Address2 + " " + Comparedata.State + " " + (Comparedata.Zip.Length > 5 ? Comparedata.Zip.Substring(0, 4) : Comparedata.Zip); //first LastName +Address1 + Addresss2 +City+State+Zip 
                    //    //        int CompareScore = BoomTown.FuzzySharp.Fuzzy.Ratio(dtl.Pattern5, Comparedata.Pattern5);
                    //    //        if (CompareScore > FileMaster.SimilarityScore)
                    //    //        {
                    //    //            String s1 = new String(dtl.Pattern5.Where(Char.IsDigit).ToArray());
                    //    //            String s2 = new String(Comparedata.Pattern5.Where(Char.IsDigit).ToArray());
                    //    //            if (s1 == s2)
                    //    //            {
                    //    //                Comparedata.HouseHoldId = "HH" + dtl.Memberid;
                    //    //                Comparedata.Priority = 4;
                    //    //                Comparedata.MatchedPercentage = CompareScore;
                    //    //                Comparedata.IsMatch = true;
                    //    //                if (dtl.IsMatch == false)
                    //    //                {
                    //    //                    dtl.IsMatch = true;
                    //    //                }
                    //    //            }
                    //    //        }
                    //    //    }
                    //    //}

                    //}
                    #endregion

                    FileMaster.ProcessEndDate = DateTime.UtcNow;
                    db.SaveChanges();
                    foreach (ImportedFileDetail dtlnew in FileData)
                    {
                        var u = db.ImportedFileDetail.Where(p => p.Id == dtlnew.Id).Single();
                        u.MappedMemberId = dtlnew.MappedMemberId;
                        u.MatchedPercentage = dtlnew.MatchedPercentage;
                        u.Priority = dtlnew.Priority;
                        db.SaveChanges();
                    }
                    //Code to update Actual houseHoldId
                    DataHelper ds = new DataHelper();
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@FileImportId", FileId);
                    ds.ExecuteNonQuery("proc_GenerateHouseHoldId", param);
                    //Send ALert Notification to User
                    UserService userService = new UserService();
                    User objuser = userService.UserProfileByUserId(FileMaster.UserId);
                    if (objuser != null)
                    {
                        Functions fs = new Functions();
                        fs.SendAlertNotificationAsync(objuser.EmailAddress);
                    }
                }
            }
            // db.SaveChanges();

        }
        public ValidateFileResult ValidateFile(string filename, string Path)
        {
            ValidateFileResult result = new ValidateFileResult();
            try
            {
                var fi = new System.IO.FileInfo(filename);
                DataTable dt = new DataTable();
                if (fi.Extension.ToLowerInvariant() == ".xlsx" || fi.Extension.ToLowerInvariant() == ".xls")
                {
                    dt = FileImportHelper.ReadExcelFileasTable(Path);
                }
                if (fi.Extension.ToLowerInvariant() == ".csv")
                {
                    dt = FileImportHelper.ReadCSVFileAsDataTable(Path, 0, ",");
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Count < 23)
                    {
                        result.Message.Add("Total Number of Columns should be 23");
                    }
                    else if (dt.Columns.Count == 23)
                    {
                        if (dt.Columns[0].ColumnName != "MemberId")
                        {
                            result.Message.Add("Error: MemberId column is missing");
                        }
                        if (dt.Columns[1].ColumnName != "FirstName")
                        {
                            result.Message.Add("Error: FirstName column is missing");
                        }
                        if (dt.Columns[2].ColumnName != "LastName")
                        {
                            result.Message.Add("Error: LastName column is missing");
                        }
                        if (dt.Columns[3].ColumnName != "Address1")
                        {
                            result.Message.Add("Error: Address1 column is missing");
                        }
                        if (dt.Columns[4].ColumnName != "Address2")
                        {
                            result.Message.Add("Error: Address2 column is missing");
                        }
                        if (dt.Columns[5].ColumnName != "City")
                        {
                            result.Message.Add("Error: City column is missing");
                        }
                        if (dt.Columns[6].ColumnName != "State")
                        {
                            result.Message.Add("Error: State column is missing");
                        }
                        if (dt.Columns[7].ColumnName != "Zip")
                        {
                            result.Message.Add("Error: Zip column is missing");
                        }
                        if (dt.Columns[8].ColumnName != "PhoneNo")
                        {
                            result.Message.Add("Error: PhoneNo column is missing");
                        }
                        if (dt.Columns[9].ColumnName != "JoinMembers")
                        {
                            result.Message.Add("Error: JoinMembers column is missing");
                        }
                        if (dt.Columns[10].ColumnName != "Age")
                        {
                            result.Message.Add("Error: Age column is missing");
                        }
                        if (dt.Columns[11].ColumnName != "TenureInCU")
                        {
                            result.Message.Add("Error: TenureInCU column is missing");
                        }
                        if (dt.Columns[12].ColumnName != "TotalDepositProducts")
                        {
                            result.Message.Add("Error: TotalDepositProducts column is missing");
                        }
                        if (dt.Columns[13].ColumnName != "TotalLoanProducts")
                        {
                            result.Message.Add("Error: TotalLoanProducts column is missing");
                        }
                        if (dt.Columns[14].ColumnName != "CheckingAccount")
                        {
                            result.Message.Add("Error: CheckingAccount column is missing");
                        }
                        if (dt.Columns[15].ColumnName != "CheckingAccountBalance")
                        {
                            result.Message.Add("Error: CheckingAccountBalance column is missing");
                        }
                        if (dt.Columns[16].ColumnName != "TotalTransactionInLast6Month")
                        {
                            result.Message.Add("Error: TotalTransactionInLast6Month column is missing");
                        }
                        if (dt.Columns[17].ColumnName != "TotalTransactionAmountInLast6Month")
                        {
                            result.Message.Add("Error: TotalTransactionAmountInLast6Month column is missing");
                        }
                        if (dt.Columns[18].ColumnName != "MortgageAccount")
                        {
                            result.Message.Add("Error: MortgageAccount column is missing");
                        }
                        if (dt.Columns[19].ColumnName != "MortgageBalanceLeft")
                        {
                            result.Message.Add("Error: MortgageBalanceLeft column is missing");
                        }
                        if (dt.Columns[20].ColumnName != "MortgageTermLeft")
                        {
                            result.Message.Add("Error: MortgageTermLeft column is missing");
                        }
                        if (dt.Columns[21].ColumnName != "SavingAccount")
                        {
                            result.Message.Add("Error: SavingAccount column is missing");
                        }
                        if (dt.Columns[22].ColumnName != "SavingAccountBalance")
                        {
                            result.Message.Add("Error: SavingAccountBalance column is missing");
                        }
                    }
                    if (result.Message.Count == 0)
                    {
                        Regex objIntPattern = new Regex("^[0-9]*$");
                        Regex objDecPattern = new Regex("^[0-9]*[.][0-9]*$");
                        Parallel.For(0, dt.Rows.Count, rowIndex =>
                         {
                             var row = dt.Rows[rowIndex];
                             if (row["Age"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["Age"].ToString()))
                                 {
                                     result.Message.Add("Error: Age should be numeric only. Line no:" + (rowIndex+2).ToString());
                                 }
                             }
                             if (row["TenureInCU"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["TenureInCU"].ToString()))
                                 {
                                     result.Message.Add("Error: TenureInCU should be numeric only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["TotalDepositProducts"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["TotalDepositProducts"].ToString()))
                                 {
                                     result.Message.Add("Error: TotalDepositProducts should be numeric only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["TotalLoanProducts"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["TotalLoanProducts"].ToString()))
                                 {
                                     result.Message.Add("Error: TotalLoanProducts should be numeric only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["CheckingAccountBalance"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["CheckingAccountBalance"].ToString()) && !objDecPattern.IsMatch(row["CheckingAccountBalance"].ToString()))
                                 {
                                     result.Message.Add("Error: CheckingAccountBalance should be Numeric/Decimal only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["TotalTransactionAmountInLast6Month"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["TotalTransactionAmountInLast6Month"].ToString()) && !objDecPattern.IsMatch(row["TotalTransactionAmountInLast6Month"].ToString()))
                                 {
                                     result.Message.Add("Error: TotalTransactionAmountInLast6Month should be Numeric/Decimal only. Line no:" + (rowIndex +2).ToString());
                                 }  
                             }
                             if (row["MortgageBalanceLeft"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["MortgageBalanceLeft"].ToString()) && !objDecPattern.IsMatch(row["MortgageBalanceLeft"].ToString()))
                                 {
                                     result.Message.Add("Error: MortgageBalanceLeft should be Numeric/Decimal only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["SavingAccountBalance"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["SavingAccountBalance"].ToString()) && !objDecPattern.IsMatch(row["SavingAccountBalance"].ToString()))
                                 {
                                     result.Message.Add("Error: SavingAccountBalance should be Numeric/Decimal only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["TotalTransactionInLast6Month"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["TotalTransactionInLast6Month"].ToString()))
                                 {
                                     result.Message.Add("Error: TotalTransactionInLast6Month should be numeric only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                             if (row["MortgageTermLeft"].ToString() != string.Empty)
                             {
                                 if (!objIntPattern.IsMatch(row["MortgageTermLeft"].ToString()))
                                 {
                                     result.Message.Add("Error: MortgageTermLeft should be numeric only. Line no:" + (rowIndex + 2).ToString());
                                 }
                             }
                         });
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message.Add("Error Message:" + ex.Message + " InnerException: " + (ex.InnerException == null ? string.Empty : ex.InnerException.Message));
            }
            return result;
        }
        public void StoreFiledatatoDB(string filename, string Path, int FileId)
        {
            var fi = new System.IO.FileInfo(filename);
            DataTable dt = new DataTable();
            if (fi.Extension.ToLowerInvariant() == ".xlsx" || fi.Extension.ToLowerInvariant() == ".xls")
            {
                dt = FileImportHelper.ReadExcelFileasTable(Path);
                foreach (DataRow row in dt.Rows)
                {
                    //var row = dt.Rows[rowIndex];
                    if (row["Age"].ToString() == "")
                    {
                        row["Age"] = 0;
                    }
                    if (row["TenureInCU"].ToString() == string.Empty)
                    {
                        row["TenureInCU"] = 0;
                    }
                    if (row["TotalDepositProducts"].ToString() == string.Empty)
                    {
                        row["TotalDepositProducts"] = 0;
                    }
                    if (row["TotalLoanProducts"].ToString() == string.Empty)
                    {
                        row["TotalLoanProducts"] = 0;
                    }
                    if (row["CheckingAccountBalance"].ToString() == string.Empty)
                    {
                        row["CheckingAccountBalance"] = 0;
                    }
                    if (row["TotalTransactionAmountInLast6Month"].ToString() == string.Empty)
                    {
                        row["TotalTransactionAmountInLast6Month"] = 0.00;
                    }
                    if (row["MortgageBalanceLeft"].ToString() == string.Empty)
                    {
                        row["MortgageBalanceLeft"] = 0;
                    }
                    if (row["SavingAccountBalance"].ToString() == string.Empty)
                    {
                        row["SavingAccountBalance"] = 0;
                    }
                    if (row["TotalTransactionInLast6Month"].ToString() == string.Empty)
                    {
                        row["TotalTransactionInLast6Month"] = 0;
                    }
                    if (row["MortgageTermLeft"].ToString() == string.Empty)
                    {
                        row["MortgageTermLeft"] = 0;
                    }
                }

                var duplicates =dt.AsEnumerable().GroupBy(i => new { Name = i.Field<string>("MemberId") }).Where(g => g.Count() > 1).Select(g => new { g.Key.Name }).ToList();
            }
            if (fi.Extension.ToLowerInvariant() == ".csv")
            {
                dt = FileImportHelper.ReadCSVFileAsDataTable(Path, 0, ",");
            }
            DataHelper ds = new DataHelper();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ImportData", dt);
            param[1] = new SqlParameter("@FileImportId", FileId);
            ds.ExecuteNonQuery("proc_InsertFileImportDetail", param);
        }
    }
}
