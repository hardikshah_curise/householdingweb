﻿using CU.HouseHolding.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using EF = CU.HouseHolding.Model;
namespace CU.HouseHolding.DataAccess
{
    public class CustomMappingservices : ConnectionHelper
    {
        EF.HouseHoldingEntities db = null;
        public CustomMappingservices()
        {
            db = new EF.HouseHoldingEntities(EntityConnectionString);
        }
        public CustomMappingservices(ObjectContext context)
        {
            db = context as EF.HouseHoldingEntities;
        }
        public ObjectContext DbContext
        {
            get
            {
                return db as ObjectContext;
            }
        }
        public List<string> ReportNames()
        {
            var reportnames = from r in db.ImportedFileMaster
                              select r.ReportName;
            List<string> rptname = reportnames.ToList();
            return rptname;

        }
        public List<string> Cities()
        {
            var cities = from r in db.ImportedFileDetail
                         select r.City;
            List<string> citynames = cities.Distinct().ToList();
            return citynames;

        }
        public List<string> States()
        {
            var states = from r in db.ImportedFileDetail
                         select r.State;
            List<string> statenames = states.Distinct().ToList();
            return statenames;

        }
        public List<string> ZipCodes()
        {
            var zipcodes = from r in db.ImportedFileDetail
                           select r.Zip;
            List<string> zipcodlst = zipcodes.Distinct().ToList();
            return zipcodlst;

        }
        public List<string> HouseHoldingIds(string ReportName, string City, string State, string Zip, string Membername, string HHId)
        {
            List<string> HouseHoldIds = new List<string>();
            List<EF.ImportedFileDetail> lstdetails = new List<EF.ImportedFileDetail>();
            var FileId = (from r in db.ImportedFileMaster
                          where r.ReportName == ReportName
                          select r.Id).FirstOrDefault();
            if (ReportName == "")
            {
                lstdetails = (from r in db.ImportedFileDetail
                              where r.HouseHoldId != null
                              select r).Distinct().ToList();
            }
            else
            {
                lstdetails = (from r in db.ImportedFileDetail
                              where r.ImportedFileid == FileId && r.HouseHoldId != null
                              select r).Distinct().ToList();
            }

            if (!string.IsNullOrEmpty(City))
            {
                lstdetails = lstdetails.Where(p => p.City == City).ToList();
            }
            if (!string.IsNullOrEmpty(State))
            {
                lstdetails = lstdetails.Where(p => p.State == State).ToList();
            }
            if (!string.IsNullOrEmpty(Zip))
            {
                lstdetails = lstdetails.Where(p => p.Zip == Zip).ToList();
            }
            if (!string.IsNullOrEmpty(Membername))
            {
                lstdetails = lstdetails.Where(p => p.FirstName.Contains(Membername) || p.LastName.Contains(Membername)).ToList();
            }
            if (!string.IsNullOrEmpty(HHId))
            {
                lstdetails = lstdetails.Where(p => p.HouseHoldId.Contains(HHId)).ToList();
            }
            HouseHoldIds = (from c in lstdetails
                            select c.HouseHoldId).Distinct().ToList();
            return HouseHoldIds;

        }
        public List<SearchResults> GetHouseholdDetails(string ReportName, string MemberId, string MemberName, string HouseHoldId, string HOHId)
        {
            List<string> HouseHoldIds = new List<string>();
            List<EF.ImportedFileDetail> lstdetails = new List<EF.ImportedFileDetail>();
            var FileId = (from r in db.ImportedFileMaster
                          where r.ReportName == ReportName
                          select r.Id).FirstOrDefault();
            if (ReportName == "")
            {
                lstdetails = (from r in db.ImportedFileDetail
                              where r.HouseHoldId != null
                              select r).Distinct().ToList();
            }
            else
            {
                lstdetails = (from r in db.ImportedFileDetail
                              where r.ImportedFileid == FileId && r.HouseHoldId != null
                              select r).Distinct().ToList();
            }

            if (!string.IsNullOrEmpty(MemberId))
            {
                lstdetails = lstdetails.Where(p => p.Memberid == MemberId).ToList();
            }
            if (!string.IsNullOrEmpty(HouseHoldId))
            {
                lstdetails = lstdetails.Where(p => p.HouseHoldId == HouseHoldId).ToList();
            }
            if (!string.IsNullOrEmpty(MemberName))
            {
                lstdetails = lstdetails.Where(p => p.FirstName.ToLower().Contains(MemberName.ToLower()) || p.LastName.ToLower().Contains(MemberName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(HOHId))
            {
                lstdetails = lstdetails.Where(p => (p.Memberid == MemberId || p.HouseHoldId == HOHId) && p.HOH == true).ToList();
            }
            var lst = (from c in lstdetails
                       select new SearchResults { Address = c.Address1 + " " + c.Address2, MemberId = c.Memberid, HouseHoldId = c.HouseHoldId, MemberName = c.FirstName + " " + c.LastName }).Distinct().ToList();
            return lst;

        }
        public List<ImportedFileDetail> UnAllocatedMembers(int pageCount, int takeCount)
        {

            //List<string> MemberIds = new List<string>();
            var Members = (from r in db.ImportedFileDetail
                           where r.HouseHoldId == null
                           select new ImportedFileDetail { Id = r.Id, FirstName = r.FirstName, LastName = r.LastName, Memberid = r.Memberid }).ToList().Skip((pageCount - 1) * takeCount).Take(takeCount).ToList();

            return Members;
        }
        public List<ImportedFileDetail> UnAllocatedMembersbySearchText(string SearchText)
        {

            //List<string> MemberIds = new List<string>();
            var Members = (from r in db.ImportedFileDetail
                           where r.HouseHoldId == null && (r.FirstName.Contains(SearchText) || r.LastName.Contains(SearchText))
                           select new ImportedFileDetail { Id = r.Id, FirstName = r.FirstName, LastName = r.LastName, Memberid = r.Memberid }).Take(250).ToList();

            return Members;
        }
        public List<ImportedFileDetail> MembersbyHouseHold(string HouseHoldId)
        {
            var Members = (from r in db.ImportedFileDetail
                           where r.HouseHoldId != null && r.HouseHoldId == HouseHoldId
                           select new ImportedFileDetail { Id = r.Id, FirstName = r.FirstName, LastName = r.LastName, Memberid = r.Memberid }).ToList();

            return Members;

        }

        public void DeAllocateMembers(string MemberIds, string HouseHoldId)
        {
            foreach (var member in MemberIds.Split(','))
            {
                if (string.IsNullOrEmpty(member) || member=="checkAll") continue;
                int memberid = Convert.ToInt32(member);
                var obj = db.ImportedFileDetail.Where(r => r.Id == memberid && r.HouseHoldId == HouseHoldId).FirstOrDefault();
                if (obj != null)
                {
                    obj.MappedMemberId = null;
                    obj.HouseHoldId = null;
                }
                db.SaveChanges();
            }
        }
        public void AllocateMembers(string MemberIds, string HouseHoldId)
        {
            var obj1 = db.ImportedFileDetail.Where(r => r.HouseHoldId == HouseHoldId).FirstOrDefault();
            foreach (var member in MemberIds.Split(','))
            {
                if (string.IsNullOrEmpty(member)) continue;
                int memberid = Convert.ToInt32(member);
                var obj = db.ImportedFileDetail.Where(r => r.Id == memberid).FirstOrDefault();
                if (obj != null)
                {
                    obj.MappedMemberId = obj1.MappedMemberId;
                    obj.HouseHoldId = HouseHoldId;
                }
                db.SaveChanges();
            }
        }
        public string AllocateMembersWithNewHH(string MemberIds)
        {
            DataHelper ds = new DataHelper();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@MembersId", MemberIds.Substring(0, MemberIds.Length - 1));
            DataTable dt = ds.ExecuteStoredProcedureDataTable("proc_AllocatewithNewHouseHoldId", param);
            return dt.Rows[0][0].ToString();
        }
    }
}
