﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace CU.HouseHolding.Helper
{
    public class FileImportHelper
    {
        public static List<List<string>> ReadExcelOrCSVFile_New(string filename, int minColumns = 0)
        {

            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();

            if (new string[] { ".xlsx", ".xls" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadExcelFile(filename);

            }
            else // read as text file
            {
                rows = ReadTextFile(filename);

            }

            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }

            return rows;
        }


        public static List<List<string>> ReadExcelOrCSVFile(string filename, int minColumns = 0)
        {
            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();

            if (new string[] { ".xlsx", ".xls", ".csv" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadExcelFile(filename);

            }
            else // read as text file
            {
                rows = ReadTextFile(filename);

            }

            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }

            return rows;
        }
        public static List<List<string>> ReadExcelOrCSVFileCustom(string filename, int minColumns = 0)
        {
            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();

            if (new string[] { ".xlsx", ".xls" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadExcelFile(filename);
            }
            else if (new string[] { ".csv" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadCSVFileAsTextFile(filename, 0, ",");
            }
            else // read as text file
            {
                rows = ReadTextFileCustomSeprator(filename);

            }

            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }

            return rows;
        }

        public static List<List<string>> ReadCSVFileAsTextFile(string filename, int minColumns = 0, string Separator = "")
        {
            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();

            rows = ReadTextFileWithSeparator(filename, Separator);

            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }

            return rows;
        }

        public static DataTable ReadCSVFileAsDataTable(string filename, int minColumns = 0, string Separator = "")
        {
            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();
            List<string[]> ret = new List<string[]>();
            rows = ReadTextFileWithSeparator(filename, Separator);
            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }
            foreach (var item in rows)
            {
                if (!IsListBlank(item))
                {
                    ret.Add(item.ToArray());
                }
            }
            return ret.ToDataTable();
        }


        private static List<List<string>> ReadTextFile(string temp)
        {
            List<List<string>> ret = new List<List<string>>();

            var lines = System.IO.File.ReadAllLines(temp);

            foreach (var line in lines)
            {
                string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);

                if (!IsListBlank(values))
                {
                    ret.Add(TrimAndUpper(values.ToList()));
                }
            }
            return ret;
        }

        public static List<List<string>> ReadTextFileWithSeparator(string temp, string separator = ",")
        {
            List<List<string>> ret = new List<List<string>>();

            var lines = System.IO.File.ReadAllLines(temp);

            foreach (var line in lines)
            {
                string[] values = line.Split(new string[] { separator }, StringSplitOptions.None);

                if (!IsListBlank(values))
                {
                    ret.Add(TrimAndUpperWithRemoveQuote(values.ToList()));
                }
            }
            return ret;
        }

        private static List<string> TrimAndUpperWithRemoveQuote(List<string> list)
        {
            List<string> ret = new List<string>();

            foreach (var item in list)
            {
                ret.Add(StringHelper.TrimAndUpper(item).Replace("\"", ""));
            }
            return ret;

        }


        private static List<List<string>> ReadTextFileCustomSeprator(string temp)
        {
            List<List<string>> ret = new List<List<string>>();

            var lines = System.IO.File.ReadAllLines(temp);

            foreach (var line in lines)
            {
                // string testline=line.Replace('","','$$');
                var line1 = line.Replace("\",\"", "||");
                line1 = line1.Replace(",\"", "||");
                //line1 = line1.Replace(",", ""); // Safe to remove commas now.
                line1 = line1.Replace("\"", ""); // Get rid of left over double quotes.

                string[] values = line1.Split(new string[] { "||" }, StringSplitOptions.None);
                //string[] values = line.Split(new string[] { @""",""" }, StringSplitOptions.None);

                if (!IsListBlank(values))
                {
                    ret.Add(TrimAndUpper(values.ToList()));
                }
            }
            return ret;
        }
        public static DataTable ReadExcelFileasTable(string temp)
        {
            List<string[]> ret = new List<string[]>();

            var lines = TakeIo.Spreadsheet.Spreadsheet.Read(new System.IO.FileInfo(temp));

            foreach (var item in lines)
            {
                if (!IsListBlank(item))
                {
                    ret.Add(item.ToArray());
                }
            }

            return ret.ToDataTable();
        }

        private static List<List<string>> ReadExcelFile(string temp)
        {
            List<List<string>> ret = new List<List<string>>();

            var lines = TakeIo.Spreadsheet.Spreadsheet.Read(new System.IO.FileInfo(temp));

            foreach (var item in lines)
            {
                if (!IsListBlank(item))
                {
                    ret.Add(TrimAndUpper(item.ToList()));
                }
            }

            return ret;
        }
        private static List<string> TrimAndUpper(List<string> list)
        {
            List<string> ret = new List<string>();

            foreach (var item in list)
            {
                ret.Add(StringHelper.TrimAndUpper(item));
            }
            return ret;

        }

        private static bool IsListBlank(IEnumerable<string> list)
        {
            var result = list.Where(p => !string.IsNullOrEmpty(p)).Distinct().ToList();

            return !result.Any();
        }

        public static byte[] ReadStream(System.IO.Stream fileStream)
        {
            var mStreamer = new System.IO.MemoryStream();
            mStreamer.SetLength(fileStream.Length);
            fileStream.Read(mStreamer.GetBuffer(), 0, (int)fileStream.Length);
            mStreamer.Seek(0, System.IO.SeekOrigin.Begin);
            byte[] fileBytes = mStreamer.GetBuffer();

            return fileBytes;
        }

        public static DataTable ImportXLSX(string path)
        {
            DataTable dt = new DataTable();
            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(path, false))
            {
                //Read the first Sheet from Excel file.
                Sheet sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                //Get the Worksheet instance.
                Worksheet worksheet = (doc.WorkbookPart.GetPartById(sheet.Id.Value) as WorksheetPart).Worksheet;
                //Fetch all the rows present in the Worksheet.
                IEnumerable<Row> rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                //Loop through the Worksheet rows.
                foreach (Row row in rows)
                {
                    //Use the first row to add columns to DataTable.
                    if (row.RowIndex.Value == 1)
                    {
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            dt.Columns.Add(GetValue(doc, cell));
                        }

                        //dt.Rows.Add();
                        //int a = 0;
                        //foreach (Cell cell in row.Descendants<Cell>())
                        //{
                        //    dt.Rows[dt.Rows.Count - 1][a] = GetValue(doc, cell);
                        //    a++;
                        //}
                    }
                    else
                    {
                        //Add rows to DataTable.
                        dt.Rows.Add();
                        int i = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = GetValue(doc, cell);
                            i++;
                        }
                    }
                }
            }
            return dt;
        }

        private static string GetValue(DocumentFormat.OpenXml.Packaging.SpreadsheetDocument doc, Cell cell)
        {
            string value = string.Empty;
            if (cell.CellValue != null)
            {
                value = cell.CellValue.InnerText;
                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
                }
            }
            return value;
        }
        public static List<string[]> ConvertTable(DataTable table)
        {
            return table.Rows.Cast<DataRow>()
               .Select(row => table.Columns.Cast<DataColumn>()
                  .Select(col => Convert.ToString(row[col]))
               .ToArray())
            .ToList();
        }

        public static List<List<string>> ReadXLSXCSVCustom(string filename, int minColumns = 0) // XLSX formatted for OpenXML 
        {
            var fi = new System.IO.FileInfo(filename);
            List<List<string>> rows = new List<List<string>>();

            if (new string[] { ".xlsx" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                List<List<string>> ret = new List<List<string>>();
                DataTable dt = new DataTable();
                dt = ImportXLSX(filename);
                // var lines = ImportXLSX(new System.IO.FileInfo(temp));
                List<string[]> lstline = ConvertTable(dt);
                if (minColumns > 0)
                {
                    foreach (var item in lstline)
                    {
                        if (!IsListBlank(item))
                        {
                            ret.Add(TrimAndUpper(item.ToList()));
                        }
                    }
                }
                return ret;
            }
            else if (new string[] { ".xls" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadExcelFile(filename);
            }
            else if (new string[] { ".csv" }.Contains(fi.Extension.ToLowerInvariant()))
            {
                rows = ReadCSVFileAsTextFile(filename, 0, ",");
            }
            else // read as text file
            {
                rows = ReadTextFileCustomSeprator(filename);

            }

            if (minColumns > 0)
            {
                foreach (var item in rows)
                {
                    while (item.Count < minColumns)
                    {
                        item.Add("");
                    }
                }
            }

            return rows;
        }

    }
    static class ListExtensions
    {
        public static DataTable ToDataTable(this List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add(list[0][i]);
            }
            list.RemoveAt(0);
            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }
    }

}
