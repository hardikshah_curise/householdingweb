﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CU.HouseHolding.Helper
{
    /// <summary>
    /// Generic string related helpers
    /// </summary>
    public class StringHelper
    {
        static readonly Dictionary<string, string> AU_State_List;
        static StringHelper()
        {
            AU_State_List = new Dictionary<string, string>();
            AU_State_List.Add("NSW", "2");
            AU_State_List.Add("New South Wales", "2");
            AU_State_List.Add("VIC", "3");
            AU_State_List.Add("Victoria", "3");
            AU_State_List.Add("QLD", "4");
            AU_State_List.Add("Queensland", "4");
            AU_State_List.Add("WA", "6");
            AU_State_List.Add("Western Australia", "6");
            AU_State_List.Add("TAS", "7");
            AU_State_List.Add("Tasmaina", "7");
            AU_State_List.Add("SA", "5");
            AU_State_List.Add("South Australia", "5");
            AU_State_List.Add("ACT", "");
            AU_State_List.Add("Australian Capital Territory", "");
            AU_State_List.Add("NT", "08");
            AU_State_List.Add("Northern Territory", "08");
        }
        /// <summary>
        /// Returns the supplied string limited a max length of the limit supplied.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string StringLimit(string val, int limit)
        {
            if (string.IsNullOrEmpty(val))
                return "";
            else if (val.Length > limit)
                return val.Substring(0, limit);
            else
                return val ?? "";
        }
        /// <summary>
        /// Returns the supplied string limited a max length of the limit supplied. The result is UPPER cased
        /// </summary>
        /// <param name="val"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string StringLimitAndUpper(string val, int limit)
        {
            return TrimAndUpper(StringLimit(val, limit));
        }
        /// <summary>
        /// Returns the supplied string limited a max length of the limit supplied. The result is UPPER cased.
        /// The input string is sanitized to remote line breaks and new lines.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static string StringLimitCleanAndUpper(string val, int limit)
        {
            string str = StringLimit(val, limit).Replace("\r", "").Replace("\n", "");
            //return TrimAndUpper(CleanString2(str));
            return TrimAndUpper(str);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <param name="limit"></param>
        /// <param name="split"></param>
        /// <returns></returns>
        public static string[] StringSplitWithLimit(string val, int limit, string split)
        {
            string[] returnString = null;
            string tempString = string.Empty;
            int tempIndex;

            if (string.IsNullOrEmpty(val))
                return new string[] { "" };
            else if (val.Length > limit)
            {
                returnString = new string[2];

                tempString = StringLimit(val, limit);
                tempIndex = tempString.LastIndexOf(split);
                returnString[0] = StringLimit(val, tempIndex);
                tempIndex += 1;
                returnString[1] = val.Substring((tempIndex), (val.Length - tempIndex));

                return returnString;
            }

            else
                return new string[] { val } ?? new string[] { "" };
        }

        /// <summary>
        /// String is converted between UTF8 and ASCII and returned with the intention to remove all non acsii compatible characters.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CleanString2(string s)
        {
            string asAscii = Encoding.ASCII.GetString(
            Encoding.Convert(
                Encoding.UTF8,
                Encoding.GetEncoding(
                    Encoding.ASCII.EncodingName,
                    new EncoderReplacementFallback(" "),
                    new DecoderExceptionFallback()
                    ),
                Encoding.UTF8.GetBytes(s)
            )
                );

            return asAscii;


            //if (s != null && s.Length > 0)
            //{
            //    StringBuilder sb = new StringBuilder(s.Length);
            //    foreach (char c in s)
            //    {
            //        sb.Append(Char.IsControl(c) ? ' ' : c);                    
            //    }

            //    byte[] asciiBytes = Encoding.ASCII.GetBytes(sb.ToString());

            //    for (int i = 0; i < asciiBytes.Length; i++)
            //    {
            //        if (asciiBytes[i] < 32 || asciiBytes[i] > 127)
            //            asciiBytes[i] = 32;
            //    }

            //    return Encoding.ASCII.GetString(asciiBytes);
            //}
            //return s;
        }

        /// <summary>
        /// Cleans the string removing any non keyword typable characters
        /// Valid range are - abcdefghijklmnopqrstuvwxyz1234567890`~!@#$%^&*()_+[]\\{}|;':\",./<>?
        /// </summary>
        /// <param name="str">string to clean</param>
        /// <param name="replace">replacement character</param>
        /// <param name="blacklist">char array, lower cased, for chars to replace</param>
        /// <returns></returns>
        public static string CleanStringHuman(string str, string replace, char[] blacklist = null)
        {
            var an = "abcdefghijklmnopqrstuvwxyz1234567890`~!@#$%^&*()_+[]\\{}|;':\",./<>?";
            var alphanumerics = an.ToList();

            if (blacklist != null && blacklist.Any())
            {
                foreach (var item in blacklist)
                {
                    if (alphanumerics.Contains(item))
                        alphanumerics.Remove(item);
                }
            }

            StringBuilder sb = new StringBuilder();

            foreach (var item in str.ToLower().ToList())
            {
                sb.Append(alphanumerics.Contains(item) ? item.ToString() : " ");
            }

            return sb.ToString();
        }

        //public string RemoveIllegalXMLCharacters(string textIn)
        //{
        //    // Used to hold the output
        //    StringBuilder textOutBuilder = new StringBuilder();
        //    string sOutput = string.Empty;
        //    // Used to reference the current character.
        //    char current = '\0';
        //    //Exit out and return an empty string if nothing was passed in to method 
        //    if (textIn == null || textIn == string.Empty)
        //    {
        //        return string.Empty;
        //    }
        //    for (int i = 0; i <= textIn.Length - 1; i++)
        //    {
        //        current = textIn[i];
        //        if ((Strings.AscW(current) == 0x9 || 
        //            Strings.AscW(current) == 0xa || 
        //            Strings.AscW(current) == 0xd) || 
        //            ((Strings.AscW(current) >= 0x20) && (Strings.AscW(current) <= 0xd7ff)) || 
        //            ((Strings.AscW(current) >= 0xe000) && (Strings.AscW(current) <= 0xfffd)) || 
        //            ((Strings.AscW(current) >= 0x10000) && (Strings.AscW(current) <= 0x10ffff)))
        //        {
        //            textOutBuilder.Append(current);
        //        }
        //    }
        //    sOutput = RemoveAccents(textOutBuilder.ToString);
        //    return sOutput;
        //    //textOutBuilder.ToString()
        //}


        public static string StringLimitAndLower(string val, int limit)
        {
            return TrimAndLower(StringLimit(val, limit));
        }
        public static string StringLimit(object val, int limit)
        {
            if (val == null || string.IsNullOrEmpty(val.ToString()))
                return "";
            else if (val.ToString().Length > limit)
                return val.ToString().Substring(0, limit);
            else
                return val.ToString();
        }

        public static byte[] ToBytes(string item)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(item);
        }

        public static string BytesToString(byte[] item)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(item);
        }

        public static int IntOrZero(string str)
        {
            int i = 0;

            if (string.IsNullOrEmpty(str))
                return 0;

            int.TryParse(str, out i);
            return i;
        }

        public static bool IsNumeric(string strToCheck)
        {
            return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
        }
        public static bool IsNumericV2(string input)
        {
            return Regex.IsMatch(input, @"^\d+$");
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string ListToString<T>(List<T> list, char delimiter = '|')
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in list)
            {
                sb.Append(((T)item).ToString() + delimiter);
            }

            var str = sb.ToString().Trim();

            if (str.EndsWith(delimiter.ToString()))
                str = str.Substring(0, str.Length - 1);

            return str;
        }

        public static string CleanPhoneNumber(string item)
        {
            if (string.IsNullOrEmpty(item))
                return string.Empty;
            else
                return item.Replace(" ", "").Replace("-", "").Replace("+", "");
        }

        public static string TrimAndUpper(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
                return str.Trim().ToUpperInvariant();
        }
        public static string TrimAndLower(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
                return str.Trim().ToLowerInvariant();
        }

        public static string TrimAndUpperAndClean(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
                return CleanString2(str.Trim()).ToUpperInvariant();
        }
        public static bool TrueFalse(string str)
        {
            if (string.IsNullOrEmpty(str) || str == null)
                return false;
            else if (str.Trim().ToUpper() == "Y" || str.Trim().ToUpper() == "TRUE" || str.Trim().ToUpper() == "T" || str.Trim().ToUpper() == "YES")
                return true;
            else if (str.Trim().ToUpper() == "N" || str.Trim().ToUpper() == "FALSE" || str.Trim().ToUpper() == "F" || str.Trim().ToUpper() == "NO")
                return false;
            else
                throw new InvalidCastException("[" + str + "] not a valid bool");
        }
        /// <summary>
        /// Parses and returns the value as a bool.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool TryParseTrueFalse(string str, out bool val)
        {
            val = false;

            if (string.IsNullOrEmpty(str) || str == null)
                val = false;
            else if (str.Trim().ToUpper() == "Y" || str.Trim().ToUpper() == "TRUE" || str.Trim().ToUpper() == "T" || str.Trim().ToUpper() == "YES")
                val = true;
            else if (str.Trim().ToUpper() == "N" || str.Trim().ToUpper() == "FALSE" || str.Trim().ToUpper() == "F" || str.Trim().ToUpper() == "NO")
                val = false;
            else
                return false;
            //throw new InvalidCastException("[" + str + "] not a valid bool");

            return true;
        }

        public static bool IsValidEmail(string item)
        {
            if (string.IsNullOrEmpty(item)) return true;

            // string pattern1 = @"^((?:(?:(?:[a-zA-Z0-9][\.\-\+_]?)*)[a-zA-Z0-9])+)\@((?:(?:(?:[a-zA-Z0-9][\.\-_]?){0,62})[a-zA-Z0-9])+)\.([a-zA-Z0-9]{2,6})$";


            //        string pattern2 = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            //  + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
            //[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            //  + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
            //[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            //  + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

            //            string pattern2 = @"^(([\w+]+\.)+[\w+]+|([a-zA-Z]{1}|[\w+]{2,}))@"
            //+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
            //    [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
            //+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
            //    [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            //+ @"(\w+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

            if (item.Trim().ToLowerInvariant().EndsWith(".c")) return false;
            if (item.Trim().ToLowerInvariant().EndsWith(".")) return false;

            // -------------------------
            // EMAIL MUST BE IN FORMAT:  LOCAL @ DOMAIN
            // -------------------------
            // There must be exactly one @
            int firstAtSign = item.IndexOf("@");
            if (firstAtSign == -1)
                return false;
            string domain = item.Substring(firstAtSign + 1);
            string[] labels = domain.Split('.');
            if (labels.Length < 2)
                return false;

            string label = labels.Last();
            if (label.Length <= 1)
                return false;

            string pattern2 = @"^((?:(?:(?:[a-zA-Z0-9][\.\-\+_]{0,62}?)*)[a-zA-Z0-9]{0,62})+)\@((?:(?:(?:[a-zA-Z0-9][\.\-_]?){0,62})[a-zA-Z0-9])+)\.{1,62}([a-zA-Z0-9]{2,10})$";

            return Regex.IsMatch(item.Trim().ToLowerInvariant(), pattern2);
        }

        /// <summary>
        /// Custom number to string translation. Works with StringToNumber
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string NumberToString(int number)
        {
            var str = number.ToString("000000").ToArray();
            var result = "";

            foreach (var item in str)
            {
                switch (item)
                {
                    case '0':
                        result += "A";
                        break;
                    case '1':
                        result += "B";
                        break;
                    case '2':
                        result += "C";
                        break;
                    case '3':
                        result += "D";
                        break;
                    case '4':
                        result += "E";
                        break;
                    case '5':
                        result += "F";
                        break;
                    case '6':
                        result += "G";
                        break;
                    case '7':
                        result += "H";
                        break;
                    case '8':
                        result += "I";
                        break;
                    case '9':
                        result += "J";
                        break;
                }
            }

            return result;

        }
        /// <summary>
        /// Custom string to number translation. Works with NumberToString
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int StringToNumber(string number)
        {
            var str = number.ToList();
            var result = "";

            foreach (var item in str)
            {
                switch (item.ToString().ToUpper())
                {
                    case "A":
                        result += "0";
                        break;
                    case "B":
                        result += "1";
                        break;
                    case "C":
                        result += "2";
                        break;
                    case "D":
                        result += "3";
                        break;
                    case "E":
                        result += "4";
                        break;
                    case "F":
                        result += "5";
                        break;
                    case "G":
                        result += "6";
                        break;
                    case "H":
                        result += "7";
                        break;
                    case "I":
                        result += "8";
                        break;
                    case "J":
                        result += "9";
                        break;
                }
            }

            return int.Parse(result);

        }

        public static string Decimal_Validate(string value, string field, bool allowZero = true, bool requiresPositiveValue = false, bool blankToZero = true, bool allowblank = false)
        {

            if (string.IsNullOrEmpty(value) && allowblank)
            {
                return "";
            }

            if (string.IsNullOrEmpty(value) && blankToZero)
            {
                //return "0";
                return "";
            }

            decimal d;
            if (decimal.TryParse(value, out d))
            {
                if (requiresPositiveValue && d <= 0)
                {
                    return field + " should be > 0.00";
                }

                return "";
            }

            return field + " not a valid number";

        }


        public static string CleanString(string strIn)
        {
            // Replace invalid characters with empty strings. 
            try
            {
                return Regex.Replace(strIn, @"[^\w\.@-]", "", RegexOptions.None);
            }
            // If we timeout when replacing invalid characters,  
            // we should return Empty. 
            catch (Exception)
            {
                return strIn;
            }
        }

        /// <summary>
        /// Normalises the provided Url to start with http.  Uses HTTP only. Not coded for HTTPS
        /// </summary>
        /// <param name="customerLogoLink"></param>
        /// <returns></returns>
        public static string PrepUrl(string customerLogoLink)
        {
            if (string.IsNullOrEmpty(customerLogoLink))
                return string.Empty;
            else if (customerLogoLink.StartsWith("www"))
                return "http://" + customerLogoLink;
            else if (customerLogoLink.StartsWith("//"))
                return "http:" + customerLogoLink;
            else if (!customerLogoLink.StartsWith("http://") && !customerLogoLink.StartsWith("https://"))
                return "http://" + customerLogoLink;
            else
                return customerLogoLink;
        }



        /// <summary>
        /// Removes irregular characters from the filename and replaces them with underscore.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string SanitizeFilename(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        public static string GetCorrectStateCode_AU(string DeliveryState, string Postcode)
        {
            string result = "";
            result = AU_State_List.FirstOrDefault(p => p.Key == DeliveryState || p.Value == (Postcode.StartsWith("0") ? Postcode.Substring(0, 2) : Postcode.Substring(0, 1))).Key;
            if (string.IsNullOrEmpty(result))
                result = DeliveryState;

            return result;
        }
    }
}
