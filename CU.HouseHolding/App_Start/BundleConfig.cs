﻿using System.Web;
using System.Web.Optimization;

namespace CU.HouseHolding
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/assets/vendor/modernizr/modernizr.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/DashboardCSS").Include(
                     "~/assets/vendor/bootstrap/css/bootstrap.css",
                     "~/assets/vendor/font-awesome/css/font-awesome.css",
                     "~/assets/vendor/magnific-popup/magnific-popup.css",
                     "~/assets/vendor/bootstrap-datepicker/css/datepicker3.css",
                     "~/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css",
                     "~/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css",
                     "~/assets/vendor/morris/morris.css",
                     "~/assets/stylesheets/theme.css",
                     "~/assets/stylesheets/skins/default.css",
                     "~/assets/stylesheets/theme-custom.css"));
        }
    }
}
