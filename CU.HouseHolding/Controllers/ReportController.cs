﻿using CU.HouseHolding.DataAccess;
using CU.HouseHolding.DataAccess.Model;
using CU.HouseHolding.Helper;
using CU.HouseHolding.Models;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.text.html.simpleparser;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data;
 
using System.Linq;
using System.Web.Mvc;

namespace CU.HouseHolding.Controllers
{
    [CheckAuthorization]
    public class ReportController : BaseController
    {
        // GET: Report
        public ActionResult Index(ImportedFileMasterView model)
        {
            ImportedFileService fileService = new ImportedFileService();
            int UserId = Convert.ToInt32(Session["UserID"]);
            if (!string.IsNullOrEmpty(Request.Form["fromdate"]))
            {
                ViewBag.FromDate = Request.Form["fromdate"];
            }
            else
            {
                ViewBag.FromDate = DateTime.Now.AddDays(-7).Date.ToString("MM/dd/yyyy");
            }
            if (!string.IsNullOrEmpty(Request.Form["todate"]))
            {
                ViewBag.ToDate = Request.Form["todate"];
            }
            else
            {
                ViewBag.ToDate = DateTime.Now.Date.ToString("MM/dd/yyyy");
            }
            DateTime fromdate = Convert.ToDateTime(ViewBag.FromDate);
            DateTime todate = Convert.ToDateTime(ViewBag.ToDate).AddDays(1);
            List<ImportedFileMaster> lst = fileService.ImportedFileMasters.Where(p => p.UserId == UserId && p.CreatedDate >= fromdate && p.CreatedDate <= todate).ToList();
            model.Rows = lst.Count;

            model.Pages = (int)Math.Round(model.Rows / (model.PageSize * 1.0), 0, MidpointRounding.AwayFromZero);
            if (model.Pages == 0)
            {
                model.Pages = 1;
            }

            if (model.Page > model.Pages)
            {
                model.Page = 1;
            }
            model.FileMstResults = lst.Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();
            return View(model);
        }
        public ActionResult Detail(ImportedFileMasterView model, int Id)
        {
            ImportedFileService fileService = new ImportedFileService();
            List<ImportedFileDetail> lst = fileService.ImportedFileDetails.Where(p => p.ImportedFileid == Id).ToList();
            ViewData["ReportId"] = Id.ToString();
            model.Rows = lst.Count;
            model.Pages = (int)Math.Round(model.Rows / (model.PageSize * 1.0), 0, MidpointRounding.AwayFromZero);
            if (model.Pages == 0)
            {
                model.Pages = 1;
            }

            if (model.Page > model.Pages)
            {
                model.Page = 1;
            }
            model.FileDtlResults = lst.Skip((model.Page - 1) * model.PageSize).Take(model.PageSize).ToList();
            return View(model);
        }
        public ActionResult Search()
        {
            CustomMappingservices cs = new CustomMappingservices();
            ViewReportModel model = new ViewReportModel();
            model.ReportNames = cs.ReportNames();
            ViewBag.SelectedName = "All Reports";
            return View(model);
        }
        [HttpPost]
        public ActionResult Search(ViewReportModel model)
        {
            CustomMappingservices cs = new CustomMappingservices();
            ViewBag.SelectedName = model.ReportNames[0];

            model.ReportNames = cs.ReportNames();
            model.lstResults = cs.GetHouseholdDetails(Request.Form["ReportNames"].ToString(), Request.Form["MemberId"].ToString(), Request.Form["MemberName"].ToString(), Request.Form["HouseHoldId"].ToString(), Request.Form["HOHId"].ToString());

            return View(model);
        }
        public ActionResult Download(int Id, string FileName)
        {
            if (Id == 0)
            {
                string fullPath = Server.MapPath("~/App_Data/UploadedFiles/08042020124411_HH -Sample DAta.xlsx");
                byte[] fileByteArray = System.IO.File.ReadAllBytes(fullPath);
                //System.IO.File.Delete(fullPath);
                return File(fileByteArray, "application/vnd.ms-excel", "SampleData.xlsx");
            }
            else
            {
                DataHelper ds = new DataHelper();
                ImportedFileService fileService = new ImportedFileService();
                var filemaster= fileService.ImportedFileMasters.Where(p => p.Id == Id).FirstOrDefault();
                if (filemaster != null)
                {
                    List<ExportFileDetail> lst = fileService.ExportFileDetails(Id);
                    DataTable dt = ds.ConvertToDataTable(lst.AsEnumerable());
                    var bytes = FileExportHelper.DataTableToCSV(dt);

                    return File(bytes.ToArray(), "application/text", filemaster.ReportName + ".csv");
                }
                else
                {
                    return Content("Error");
                }
            }
        }
        public ActionResult SummaryByTotalMember()
        {
            ReportService rs = new ReportService();
            SummrybyMembers model = new SummrybyMembers();
            model.lstResults = rs.GetSummarybyMembers();
            model.ChartData = rs.GetSummarybyMembersChartData();
            ViewBag.Series = string.Join(",", model.ChartData.Select(p => p.Series).ToList());
            ViewBag.Values = string.Join(",", model.ChartData.Select(p => p.Values).ToList());
            return View(model);
        }
        public ActionResult SummaryByTotalProducts()
        {
            ReportService rs = new ReportService();
            SummrybyMembers model = new SummrybyMembers();
            model.lstResults = rs.GetSummarybyProducts();
            return View(model);
        }
        public ActionResult SummaryDetail(string Type, int Tiergroup)
        {
            ReportService rs = new ReportService();
            SummryDetailViewResult model = new SummryDetailViewResult();
            model.lstResults = rs.GetSummaryDetails(Type, Tiergroup);
            ViewBag.Type = Type;
            ViewBag.Tiergroup = Tiergroup;
            return View(model);
        }
        public ActionResult HouseHoldSummary(string HouseHoldId)
        {
            ReportService rs = new ReportService();
            ImportedFileService fs = new ImportedFileService();
            HouseHoldSummaryViewResult model = new HouseHoldSummaryViewResult();
            model.lstMembers = fs.GetMemberDetailsbyHouseHoldId(HouseHoldId);
            model.HHSummaryDetail = rs.GetHouseHoldSummarybyHouseHoldId(HouseHoldId);
            ViewBag.HouseHoldId = HouseHoldId;
            return View(model);
        }
        public ActionResult SummaryGraph()
        {
            ReportService rs = new ReportService();
            SummrybyMembers model = new SummrybyMembers();
            //model.lstResults = rs.GetSummarybyMembers();
            model.ChartData = rs.GetSummarybyMembersChartData();
            ViewBag.Series= string.Join(",", model.ChartData.Select(p=>p.Series).ToList());
            ViewBag.Values = string.Join(",", model.ChartData.Select(p => p.Values).ToList());
            return View();
        }
            public ActionResult DownloadSummary(string ReportType,string ExportType)
        {
            SummrybyMembers model = new SummrybyMembers();
            ReportService rs = new ReportService();
            if (ReportType == "Members")
            {
                model.lstResults = rs.GetSummarybyMembers();
            }
            else
            {
                model.lstResults = rs.GetSummarybyProducts();
            }
            DataHelper ds = new DataHelper();
            DataTable dt = ds.ConvertToDataTable(model.lstResults.AsEnumerable());
            if (ExportType == "CSV")
            {
                var bytes = FileExportHelper.DataTableToCSV(dt);
                return File(bytes.ToArray(), "application/text", "ExportSummaryBy" + ReportType + ".csv");
            }
            if (ExportType == "Excel")
            {
                var bytes = ExcelUtility.GetExcel(dt);
                return File(bytes.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExportSummaryBy" + ReportType + ".xlsx");
            }
            return Content("Error");
        }

        public ActionResult DownloadSummaryDetail(string ReportType, string ExportType, int Tiergroup)
        {
            ReportService rs = new ReportService();
            SummryDetailViewResult model = new SummryDetailViewResult();
            model.lstResults = rs.GetSummaryDetails(ReportType, Tiergroup);
            ViewBag.Type = ReportType;
            ViewBag.Tiergroup = Tiergroup;
            if (ExportType == "CSV")
            {
                var bytes = FileExportHelper.DataTableToCSV(model.lstResults);
                return File(bytes.ToArray(), "application/text", "SummaryDetailBy" + ReportType + "_Tier"+ Tiergroup.ToString() + ".csv");
            }
            if (ExportType == "Excel")
            {
                var bytes = ExcelUtility.GetExcel(model.lstResults);
                return File(bytes.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "SummaryDetailBy" + ReportType + "_Tier" + Tiergroup.ToString() + ".xlsx");
            }
            return Content("Error");
        }

        public ActionResult ViewPdf()
        {
            ReportService rs = new ReportService();
            SummrybyMembers model = new SummrybyMembers();
            model.lstResults = rs.GetSummarybyProducts();
            var report = new PartialViewAsPdf("~/Views/Report/SummaryByTotalMember.cshtml", model);
            return report;
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml,string FileName)
        {
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                StringReader sr = new StringReader(GridHtml);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", FileName+".pdf");
            }
        }

        [HttpPost]
        public JsonResult GetDaugnutXSeriesValues(int TierGroup)
        {
            ReportService rs = new ReportService();
            var lst = rs.GetMetricsChartData(TierGroup);
            return Json(new { results = lst }, JsonRequestBehavior.AllowGet);
        }
    }
}