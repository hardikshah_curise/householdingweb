﻿using CU.HouseHolding.DataAccess;
using CU.HouseHolding.DataAccess.Model;
using Hangfire;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
namespace CU.HouseHolding.Controllers
{
    public class BulkUploadController : BaseController
    {
        public BulkUploadController()
        { }
        // GET: BulkUpload
        public ActionResult Index()
        {
            // var user = CurrentProfile.LoggedUserProfile;
            //Functions objcontrl = new Functions();
            //objcontrl.SendAlertNotificationAsync();
            //BackgroundJob.Enqueue(() => objcontrl.SendAlertNotificationAsync());
            //ImportedFileService fileService = new ImportedFileService();
            //fileService.ProcessFile(7);
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form, HttpPostedFileBase postedFile)
        {
            using (var ts = new TransactionScope())
            {
                try
                {
                    ImportedFileService fileService = new ImportedFileService();
                    var result = fileService.CheckReportExist(form["ReportName"]);
                    if (result == true)
                    {
                        ts.Dispose();
                        TempData["Error"] = "Please enter Diffrent Report Name";
                    }
                    else
                    {
                        string slidervalue = form["listenSlider"];
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];
                            if (file.FileName.EndsWith(".xls") || file.FileName.EndsWith(".csv") || file.FileName.EndsWith(".xlsx"))
                            {
                                FileInfo fi = new FileInfo(file.FileName);
                                string Path = Server.MapPath("~/App_Data/UploadedFiles");
                                string SaveFileName = Path + "\\" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + fi.Name;
                                if (!Directory.Exists(Path))
                                {
                                    Directory.CreateDirectory(Path);
                                }
                                file.SaveAs(SaveFileName);
                                //Store File Master Record

                                ImportedFileMaster importedFileMaster = new ImportedFileMaster();
                                importedFileMaster.CreatedDate = DateTime.UtcNow;
                                importedFileMaster.FileName = fi.Name;
                                importedFileMaster.ReportName = form["ReportName"];
                                importedFileMaster.SimilarityScore = Convert.ToInt32(slidervalue);
                                importedFileMaster.UserId = Convert.ToInt32(Session["UserID"].ToString());
                                fileService.ImportedFileMaster_InsertOrUpdate(importedFileMaster);
                                //store Details file records 
                                fileService.StoreFiledatatoDB(fi.Name, SaveFileName, importedFileMaster.Id);
                                // Put the Algorithm to run in Background
                                //fileService.ProcessFile(importedFileMaster.Id);
                                BackgroundJob.Enqueue(() => fileService.ProcessFile(importedFileMaster.Id));
                                //return Json(fi.Name, JsonRequestBehavior.AllowGet);
                                TempData["Message"] = "File Imported Successfully and we have started our HousHolding Algorithm to processs. You will be notified via Email once report is ready";
                                ts.Complete();
                                return View();
                            }
                            else
                            {
                                TempData["Error"] = "Invalid File Type";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ts.Dispose();
                    TempData["Error"] = ex.Message;
                }
            }
            return View();
        }
        public ActionResult ValidateFile()
        {
            string FileName = "";
            HttpFileCollectionBase files = Request.Files;
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = files[i];
                string fname;

                // Checking for Internet Explorer    
                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    fname = testfiles[testfiles.Length - 1];
                }
                else
                {
                    fname = file.FileName;
                    FileName = file.FileName;
                }

                // Get the complete folder path and store the file inside it.    
                fname = Path.Combine(Server.MapPath("~/App_Data/UploadedFiles/"), fname);
                file.SaveAs(fname);
                ImportedFileService fs = new ImportedFileService();
                var result = fs.ValidateFile(new FileInfo(file.FileName).Name, fname);
                var converted = JsonConvert.SerializeObject(result.Message, Formatting.None);
                return Json(converted, JsonRequestBehavior.AllowGet);
            }

            return Json(FileName, JsonRequestBehavior.AllowGet);
        }

        #region Custom Mapping
        public ActionResult CustomMapping()
        {
            CustomMappingservices cs = new CustomMappingservices();
            CustomMappingViewModel model = new CustomMappingViewModel();
            model.ReportNames = cs.ReportNames();
            model.Cities = cs.Cities();
            model.States = cs.States();
            model.Zipcodes = cs.ZipCodes();
            //model.HouseHoldIds = new List<string>(); //cs.HouseHoldingIds(Request.Form["ReportNames"] == null ? "" : Request.Form["ReportNames"].ToString());
            //model.UnAllocatedMemberIds = cs.UnAllocatedMembers();
            ViewBag.SelectedName = "All Reports";
            ViewBag.SelectedCityName = "All Cities";
            ViewBag.SelectedStateName = "All States";
            ViewBag.SelectedZipcode = "All ZipCode";
            return View(model);
        }
        public JsonResult GetUnAllocateMembersList(int pageCount, int takeCount)

        {
            CustomMappingservices cs = new CustomMappingservices();
            var lstmembers = cs.UnAllocatedMembers(pageCount, takeCount);
            return Json(new { items = lstmembers }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFilterMembersList(string searchText)
        {
            CustomMappingservices cs = new CustomMappingservices();
            var lstmembers = cs.UnAllocatedMembersbySearchText(searchText);
            if (lstmembers.Count == 0)
                lstmembers = null;
            return Json(new { results = lstmembers }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CustomMapping(CustomMappingViewModel model)
        {
            CustomMappingservices cs = new CustomMappingservices();
            ViewBag.SelectedName = model.ReportNames[0];
            ViewBag.SelectedCityName = model.Cities[0];
            ViewBag.SelectedStateName = model.States[0];
            ViewBag.SelectedZipcode = model.Zipcodes[0];
            model.ReportNames = cs.ReportNames();
            model.Cities = cs.Cities();
            model.States = cs.States();
            model.Zipcodes = cs.ZipCodes();
            model.HouseHoldIds = cs.HouseHoldingIds(Request.Form["ReportNames"].ToString(), Request.Form["Cities"].ToString(), Request.Form["States"].ToString(), Request.Form["Zipcodes"].ToString(), Request.Form["MemberName"].ToString(), Request.Form["HHId"].ToString());
            //model.UnAllocatedMemberIds = cs.UnAllocatedMembers();
            return View(model);
        }
        public ActionResult _MembersList(string HouseHoldId)
        {
            CustomMappingservices cs = new CustomMappingservices();
            CustomMappingViewModel model = new CustomMappingViewModel();
            model.MemberIds = cs.MembersbyHouseHold(HouseHoldId);
            ViewBag.HouseHoldId = HouseHoldId;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_MembersList", model);
            }
            return View(model);
        }
        public ActionResult DeAllocateMember(string SelectedMembers, string HouseHoldId)
        {
            CustomMappingservices cs = new CustomMappingservices();
            cs.DeAllocateMembers(SelectedMembers, HouseHoldId);
            return Content("Success");
        }
        public ActionResult AllocateMember(string SelectedMembers, string HouseHoldId)
        {
            CustomMappingservices cs = new CustomMappingservices();
            cs.AllocateMembers(SelectedMembers, HouseHoldId);
            return Content("Success");
        }
        public ActionResult AllocateMemberwithNewHH(string SelectedMembers)
        {
            CustomMappingservices cs = new CustomMappingservices();
            var HHID = cs.AllocateMembersWithNewHH(SelectedMembers);
            return Content(HHID);
        }

        #endregion
    }
}