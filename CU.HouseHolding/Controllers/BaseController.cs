﻿using CU.HouseHolding.Models;
using System.Web.Mvc;

namespace CU.HouseHolding.Controllers
{
    [CheckAuthorization]
    public abstract class BaseController : Controller
    {
        // GET: Base
        public BaseController()
        {
        }
        private SessionProfile _currentProfile;
        public SessionProfile CurrentProfile
        {
            get
            {
                if (_currentProfile == null)
                {
                    _currentProfile = ProfileHelper.Profile;
                }

                return _currentProfile;
            }
        }
    }
}