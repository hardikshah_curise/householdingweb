﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CU.HouseHolding.Areas.Admin.Models;
using CU.HouseHolding.Controllers;
using CU.HouseHolding.DataAccess;
using Hangfire;
namespace CU.HouseHolding.Areas.Admin.Controllers
{
    public class HangfireJobsController : BaseController
    {
        // GET: Admin/HangfireJobs
        public ActionResult Index()
        {
            return View(new Models.HangFireJobsViewModel());
        }

        [HttpPost]
        public ActionResult Index(List<int> selected)
        {
            if (selected != null && selected.Any())
            {
                foreach (var item in selected)
                {
                    // minutes, hours, days, months, days of week
                    switch ((HangFireJobsViewModel.JobEnum)item)
                    {
                        case HangFireJobsViewModel.JobEnum.ProcessFile:
                            RecurringJob.AddOrUpdate<ImportedFileService>("Process File", ss => ss.ProcessFile(0),Cron.Hourly());
                            break;
                        //case HangFireJobsViewModel.JobEnum.SendEmail:
                        //    RecurringJob.AddOrUpdate<Functions>("Send Email", fs => fs.SendAlertNotificationAsync(), Cron.Hourly());
                        //    break;
                    }
                }

                return RedirectToAction("recurring", "hangfire", new { area = "" });
            }


            return Content("Completed");
        }
    }
}