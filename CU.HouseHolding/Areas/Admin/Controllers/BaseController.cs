﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CU.HouseHolding.Areas.Admin.Models;
namespace CU.HouseHolding.Areas.Admin.Controllers
{
    [CheckAuthorization]
    public abstract class BaseController : Controller
    {
        // GET: Admin/Base
        public BaseController()
        {
        }
        private SessionProfile _currentProfile;
        public SessionProfile CurrentProfile
        {
            get
            {
                if (_currentProfile == null)
                {
                    _currentProfile = ProfileHelper.AdminProfile;
                }

                return _currentProfile;
            }
        }
    }
}