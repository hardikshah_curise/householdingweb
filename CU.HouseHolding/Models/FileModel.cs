﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CU.HouseHolding.Models
{
    public class FileModel
    {
        //[Required(ErrorMessage = "Please select file.")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase PostedFile { get; set; }
    }
}